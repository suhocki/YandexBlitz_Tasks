package app.suhocki.star;

public class Star {

    private static final String INPUT = "0 0 100 100";
    private static final int COUNT_OF_ANGLES = 7;

    public static class Point {
        double x;
        double y;

        Point(double x, double y) {
            this.x = x;
            this.y = y;
        }
    }

    public static void main(String[] args) {
        String[] strings = INPUT.split(" ");
        double width = Double.parseDouble(strings[2]);
        double height = Double.parseDouble(strings[3]);
        double halfWidth = width / 2;
        double halfHeight = height / 2;

        Point rectangleCenter = new Point(halfWidth + Double.parseDouble(strings[0]),
                halfHeight + Double.parseDouble(strings[1]));

        double diameter = (width <= height ? width : height)-2;
        double bigRadius = diameter / (Math.cos(Math.PI / COUNT_OF_ANGLES) + 1);
        Point starCenter = new Point(Math.round(rectangleCenter.x), Math.round(rectangleCenter.y + (bigRadius - diameter / 2)));
        Point[] points = getPoints(starCenter, bigRadius, COUNT_OF_ANGLES);
        Point[] rotated = getRotatedPoints(points, starCenter, 2*Math.PI / COUNT_OF_ANGLES);
        Point[] result = new Point[]{
                points[0],
                crossLine(points[0], points[2], points[1], points[6]),
                points[1],
                crossLine(points[0], points[2], points[1], points[3]),
                points[2],
                crossLine(points[2], points[4], points[1], points[3]),
                points[3],
                crossLine(points[2], points[4], points[3], points[5]),
                points[4],
                crossLine(points[3], points[5], points[4], points[6]),
                points[5],
                crossLine(points[4], points[6], points[0], points[5]),
                points[6],
                crossLine(points[6], points[1], points[5], points[0]),

        };
        StringBuilder sb = new StringBuilder();
        for (Point point : result) {
            sb.append(Math.round(point.x));
            sb.append(" ");
            sb.append(Math.round(point.y));
            sb.append(" ");
        }
        String toString = sb.toString();
        String result1 = toString.substring(0, toString.length() - 1);
        System.out.println(result1);
        System.out.println("50 1 65 21 90 21 85 45 100 64 78 75 72 99 50 88 28 99 22 75 0 64 15 45 10 21 35 21");
        System.out.println(toString.substring(0, toString.length() - 1)
                .equals("50 1 65 21 90 21 85 45 100 64 78 75 72 99 50 88 28 99 22 75 0 64 15 45 10 21 35 21"));
    }

    private static Point[] getRotatedPoints(Point[] points, Point c, double angle) {
        Point[] rotated = new Point[COUNT_OF_ANGLES];
        for (int i = 0; i < COUNT_OF_ANGLES; i++) {
            Point p = new Point(points[i].x, points[i].y);
            p.x -= c.x;
            p.y -= c.y;

            double newx = p.x * Math.cos(angle) - p.y * Math.sin(angle);
            double newy = p.x * Math.sin(angle) + p.y * Math.cos(angle);

            p.x = newx + c.x;
            p.y = newy + c.y;
            rotated[i] = new Point(newx, newy);
        }
        return rotated;
    }

    private static Point[] getPoints(Point center, double radius, int noOfDividingPoints) {
        double angle;
        Point[] points = new Point[noOfDividingPoints];
        for (int i = 0; i < noOfDividingPoints; i++) {
            angle = i * (360.0 / noOfDividingPoints) - 90;
            points[i] = new Point(
                    center.x + radius * Math.cos(Math.toRadians(angle)),
                    center.y + radius * Math.sin(Math.toRadians(angle))
            );
        }
        return points;
    }

    private static Point crossLine(Point l1a, Point l1b, Point l2a, Point l2b) {
        double a1 = l1a.y - l1b.y;
        double b1 = l1b.x - l1a.x;
        double a2 = l2a.y - l2b.y;
        double b2 = l2b.x - l2a.x;

        double d = a1 * b2 - a2 * b1;
        if (d != 0) {
            double c1 = l1b.y * l1a.x - l1b.x * l1a.y;
            double c2 = l2b.y * l2a.x - l2b.x * l2a.y;

            double xi = (b1 * c2 - b2 * c1) / d;
            double yi = (a2 * c1 - a1 * c2) / d;
            return new Point(xi, yi);
        }
        return new Point(0d, 0d);
    }
}

