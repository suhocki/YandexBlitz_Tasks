package app.suhocki.kotlinmodule

fun main(args: Array<String>) {
    /* Enter your code here. Read input from STDIN. Print output to STDOUT. */
    val inputStringoInts = readLine()!!.split(" ")
    var answer = 0
    inputStringoInts.forEach { answer += it.toInt() }
    println(answer.toString())
}
